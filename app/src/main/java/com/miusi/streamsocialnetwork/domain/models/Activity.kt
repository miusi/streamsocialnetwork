package com.miusi.streamsocialnetwork.domain.models

import com.miusi.streamsocialnetwork.domain.util.ActivityAction

data class Activity(
    val username: String,
    val actionType: ActivityAction,
    val formattedTime: String,
)