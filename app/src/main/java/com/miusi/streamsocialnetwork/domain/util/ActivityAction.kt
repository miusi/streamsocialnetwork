package com.miusi.streamsocialnetwork.domain.util

sealed class ActivityAction {
    object LikedPost: ActivityAction()
    object CommentedOnPost: ActivityAction()
}