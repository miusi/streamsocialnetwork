package com.miusi.streamsocialnetwork.presentation

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Description
import androidx.compose.material.icons.filled.PersonAdd
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.miusi.streamsocialnetwork.R
import com.miusi.streamsocialnetwork.domain.models.User
import com.miusi.streamsocialnetwork.presentation.components.StandardTextField
import com.miusi.streamsocialnetwork.presentation.components.StandardToolbar
import com.miusi.streamsocialnetwork.presentation.components.UserProfileItem
import com.miusi.streamsocialnetwork.presentation.ui.theme.IconSizeMedium
import com.miusi.streamsocialnetwork.presentation.ui.theme.SpaceLarge
import com.miusi.streamsocialnetwork.presentation.ui.theme.SpaceMedium
import com.miusi.streamsocialnetwork.presentation.ui.theme.SpaceSmall
import com.miusi.streamsocialnetwork.presentation.util.Screen
import com.miusi.streamsocialnetwork.presentation.util.states.StandardTextFieldState

@ExperimentalMaterialApi
@Composable
fun PersonListScreen(
    navController: NavController
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        StandardToolbar(
            navController = navController,
            showBackArrow = true,
            title = {
                Text(
                    text = stringResource(id = R.string.liked_by),
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.onBackground
                )
            }
        )
        LazyColumn(
            modifier = Modifier.fillMaxSize(), contentPadding = PaddingValues(SpaceLarge)
        ) {
            items(10) {
                UserProfileItem(
                    user = User(
                        profilePictureUrl = "",
                        username = "Miusi",
                        description = "Lorem ipsum dolor sit amet consectetur adipiscing elit proin, suspendisse per " +
                                "vivamus accumsan diam egestas parturient imperdiet auctor, natoque aliquet ridiculus et " +
                                "facilisis primis ligula. ",
                        followerCount = 145,
                        followingCount = 134,
                        postCount = 65
                    ),
                    actionIcon = {
                        Icon(
                            imageVector = Icons.Default.PersonAdd,
                            contentDescription = null,
                            tint = MaterialTheme.colors.onBackground,
                            modifier = Modifier.size(IconSizeMedium)
                        )
                    }
                )
                Spacer(modifier = Modifier.height(SpaceMedium))
            }
        }
    }
}