package com.miusi.streamsocialnetwork.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val DarkGray = Color(0xFF202020)
val MediumGray = Color(0XFF404040)
val LightGray = Color(0xFF606060)
val TextWhite = Color(0XFFEEEEEE)
val HintGray = Color(0xFF6D6D6D)
val TextGray = Color(0XFFA6A6A6)
val GreenAccent = Color(0xFF08FF04)