package com.miusi.streamsocialnetwork.presentation.util

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.miusi.streamsocialnetwork.domain.models.Post
import com.miusi.streamsocialnetwork.presentation.edit_profile.EditProfileScreen
import com.miusi.streamsocialnetwork.presentation.MainFeedScreen
import com.miusi.streamsocialnetwork.presentation.PersonListScreen
import com.miusi.streamsocialnetwork.presentation.activity.ActivityScreen
import com.miusi.streamsocialnetwork.presentation.chat.ChatScreen
import com.miusi.streamsocialnetwork.presentation.create_post.CreatePostScreen
import com.miusi.streamsocialnetwork.presentation.login.LoginScreen
import com.miusi.streamsocialnetwork.presentation.post_detail.PostDetailScreen
import com.miusi.streamsocialnetwork.presentation.register.RegisterScreen
import com.miusi.streamsocialnetwork.presentation.splash.SplashScreen
import com.miusi.streamsocialnetwork.presentation.profile.ProfileScreen
import com.miusi.streamsocialnetwork.presentation.search.SearchScreen

@ExperimentalMaterialApi
@Composable
fun Navigation(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.PersonListScreen.route,
        modifier = Modifier.fillMaxSize()
    ) {
        composable(Screen.SplashScreen.route) {
            SplashScreen(navController = navController)
        }
        composable(Screen.LoginScreen.route) {
            LoginScreen(navController = navController)
        }
        composable(Screen.RegisterScreen.route) {
            RegisterScreen(navController = navController)
        }
        composable(Screen.MainFeedScreen.route) {
            MainFeedScreen(navController = navController)
        }
        composable(Screen.ChatScreen.route) {
            ChatScreen(navController = navController)
        }
        composable(Screen.ActivityScreen.route) {
            ActivityScreen(navController = navController)
        }
        composable(Screen.ProfileScreen.route) {
            ProfileScreen(navController = navController)
        }
        composable(Screen.EditProfileScreen.route) {
            EditProfileScreen(navController = navController)
        }
        composable(Screen.CreatePostScreen.route) {
            CreatePostScreen(navController = navController)
        }
        composable(Screen.SearchScreen.route) {
            SearchScreen(navController = navController)
        }
        composable(Screen.PersonListScreen.route) {
            PersonListScreen(navController = navController)
        }
        composable(Screen.PostDetailScreen.route) {
            PostDetailScreen(
                navController = rememberNavController(),
                post = Post(
                    username = "Miusi",
                    imageUrl = "",
                    profilePictureUrl = "",
                    description = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\n" +
                            "\n",
                    likeCount = 17,
                    commentCount = 7

                )
            )
        }
    }
}