package com.miusi.streamsocialnetwork.presentation.util.states

data class StandardTextFieldState(
    val text: String = "",
    val error: String = ""
)