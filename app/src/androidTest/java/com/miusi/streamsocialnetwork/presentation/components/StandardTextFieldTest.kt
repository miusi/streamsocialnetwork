package com.miusi.streamsocialnetwork.presentation.components

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performTextClearance
import androidx.compose.ui.test.performTextInput
import androidx.compose.ui.text.input.KeyboardType
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.miusi.streamsocialnetwork.presentation.MainActivity
import com.miusi.streamsocialnetwork.presentation.util.TestTags
import com.miusi.streamsocialnetwork.presentation.util.TestTags.PASSWORD_TOGGLE
import com.miusi.streamsocialnetwork.presentation.util.TestTags.STANDARD_TEXT_FIELD
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class StandardTextFieldTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUp() {

    }

    private fun setupNormalTextField() {

    }

    @Test
    fun enterTooLongString_maxLengthNoteExceeded() {
        composeTestRule.setContent {
            MaterialTheme {
                var text by remember {
                    mutableStateOf("")
                }
                StandardTextField(text = text,
                    onValueChange = {
                        text = it
                    },
                    maxLength = 5,
                    modifier = Modifier
                        .semantics {
                            testTag = STANDARD_TEXT_FIELD
                        }
                )
            }
        }

        val expectedString = "aaaaa"
        composeTestRule
            .onNodeWithTag(STANDARD_TEXT_FIELD)
            .performTextClearance()

        composeTestRule
            .onNodeWithTag(STANDARD_TEXT_FIELD)
            .performTextInput(expectedString)
        composeTestRule
            .onNodeWithTag(STANDARD_TEXT_FIELD)
            .performTextInput("a")

        composeTestRule
            .onNodeWithTag(STANDARD_TEXT_FIELD)
            .assertTextEquals(expectedString)
    }

//    @Test
//    fun enterPassword_toggleVisibility_passwordVisible(){
//        composeTestRule.setContent {
//            MaterialTheme {
//                var text by remember {
//                    mutableStateOf("")
//                }
//                StandardTextField(text = text,
//                    onValueChange = {
//                        text = it
//                    },
//                    maxLength = 5,
//                    keyboardType = KeyboardType.Password,
//                    modifier = Modifier
//                        .semantics {
//                            testTag = STANDARD_TEXT_FIELD
//                        }
//                )
//            }
//        }
//        composeTestRule
//            .onNodeWithTag(STANDARD_TEXT_FIELD)
//            .performTextInput("aaaaa")
//    }

}